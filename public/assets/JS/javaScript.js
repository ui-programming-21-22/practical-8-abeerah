
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext('2d');


  const scale = 4;
  
  const FACING_DOWN = 0;
  const FACING_UP = 1;
  const FACING_LEFT = 2;
  const FACING_RIGHT = 3;
  const FRAME_LIMIT = 12;
  const speed = 4;
  
  let keyPresses = {};
  let currentDirection = FACING_DOWN;
  let currentLoopIndex = 0;
  let frameCount = 0;
  let positionX = 330;
  let positionY = 15;

  let hasMoved = false;

  let img = new Image();
  
  
  
  function loadImage()
  {
    img.src = "assets/img/Green-16x18-spritesheet.png";
    img.onload = function() {
      window.requestAnimationFrame(gameLoop);
    };
  }

  const width = 16;
  const height = 18;
  const scaled_width = scale * width;
  const scaled_height = scale * height;
  const walkloop = [0, 1, 0, 2];
  
  function drawFrame(frameX, frameY, canvasX, canvasY) {
    context.drawImage(img,
                  frameX * width, frameY * height, width, height,
                  canvasX, canvasY, scaled_width, scaled_height);
  }
  loadImage();
  

  function GamerInput(input) {
    this.action = input; // Hold the current input as a string
  }

  // Default GamerInput is set to None
  let gamerInput = new GamerInput("None"); //No Input

  function input(event) {
 
    if (event.type === "keydown") {
        switch (event.keyCode) {
            case 65: // Left Arrow
                gamerInput = new GamerInput("Left");
                break; //Left key
            case 87: // Up Arrow
                gamerInput = new GamerInput("Up");
                break; //Up key
            case 68: // Right Arrow
                gamerInput = new GamerInput("Right");
                break; //Right key
            case 83: // Down Arrow
                gamerInput = new GamerInput("Down");
                break; //Down key
            case 32:
                gamerInput = new GamerInput ("Space");
                break;    
            case 13:
                gamerInput = new GamerInput ("Enter");
            break;    
                    
            default:
                gamerInput = new GamerInput("None"); //No Input
        }
    } else {
        gamerInput = new GamerInput("None");
    }
  }
  function update(){
    if (gamerInput.action === "Up") {
      moveCharacter(0, -speed, FACING_UP);
      hasMoved = true;
    }
    else if (gamerInput.action === "Down")
     {
      moveCharacter(0, speed, FACING_DOWN);
      hasMoved = true;
    }
    else if (gamerInput.action === "Left") 
    {
      moveCharacter(-speed, 0, FACING_LEFT);
      hasMoved = true;
    } 
    else if (gamerInput.action === "Right")
    {
      moveCharacter(speed, 0, FACING_RIGHT);
      hasMoved = true;
    }

  }

  if(positionX >= 230 && positionY >=250)
  {
    console.log("positionnnnnnnn")
    scale == 10;
  }

  function gameLoop() {
    context.clearRect(0, 0, canvas.width, canvas.height);
  
    hasMoved = false;
    update();
  
    if (hasMoved) {
      frameCount++;
      if (frameCount >= FRAME_LIMIT)
      {
        frameCount = 0;
        currentLoopIndex++;
        if (currentLoopIndex >= walkloop.length) 
        {
          currentLoopIndex = 0;
        }
      }
    }
    
    if (!hasMoved) {
      currentLoopIndex = 0;
    }
  
    drawFrame(walkloop[currentLoopIndex], currentDirection, positionX, positionY);
    window.requestAnimationFrame(gameLoop);
  }
  
  function moveCharacter(deltaX, deltaY, direction) {
    if (positionX + deltaX > 0 && positionX + scaled_width + deltaX < canvas.width) {
      positionX += deltaX;
    }
    if (positionY + deltaY > 0 && positionY + scaled_height + deltaY < canvas.height) {
      positionY += deltaY;
    }
    currentDirection = direction;
  }


  window.addEventListener('keydown', input);
  window.addEventListener('keyup', input);
